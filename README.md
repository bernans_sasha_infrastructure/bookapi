# Exercice API Flask et Docker

Ce projet est le départ des exercices 'API Flask et Docker' 2.1 et 2.2 de la semaine 2 dans le cours d'Infrastructure.

## Installation

1. Cloner le projet dans votre environnement
2. Installer les préalables (le fichier requirements.txt)
3. Lancer l'application avec python3 directement

## Exercice 2.1

Dans le code:
1. Ajouter une méthode getAllAuthors, qui retourne une liste de tous les auteurs (sans doublons) triée en ordre alphabétique
2. Créer méthode avec une route '/authors/all' qui retourne la liste de getAllAuthors

## Partie 2.2

Déploiement dans Docker:
1. Générer un dockerfile pour tester votre projet dans docker.
2. Lancer le conteneur et tester votre projet.

## License
[MIT](https://choosealicense.com/licenses/mit/)
