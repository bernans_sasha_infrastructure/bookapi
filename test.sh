python3 -m unittest discover -s project -v
if [ $? -ne 0 ]
then
	echo « unittest failed, aborting »
	exit 1
fi
docker stop book_cnt_cb
docker rm book_cnt_cb
docker image rm book_img_cb
docker build -t book_img_cb -f ./project/docker/Dockerfile .
docker run -p 15556:5556 --name book_cnt_cb book_img_cb
