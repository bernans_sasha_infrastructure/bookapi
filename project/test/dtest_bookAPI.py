import unittest
import requests
import json

from main import bookapi

IP = "127.0.0.1"
PORT = "15555"

class DeployementTest(unittest.TestCase):

	def test_hello(self):
		response = requests.get("http://" + IP + ":" + PORT + "/")
		self.assertEqual(200, response.status_code)
		self.assertIn('A simple API with books', response.content.decode("utf-8"))
	def test_books(self):
		response = requests.get("http://" + IP + ":" + PORT + "/" + "books")
		self.assertEqual(200, response.status_code)
		self.assertIn('James Barclay', response.content.decode("utf-8"))
		self.assertIn('author', response.content.decode("utf-8"))
		self.assertIn('{\n  "0": {\n    "author": "James Barclay",\n    "id": 0,\n    "rating": "veryGood",\n    "series": "The chronicles of the raven",\n    "title": "Dawnthief",\n    "year_published": 2003\n  },\n  "1": {\n    "author": "Raymond Feist",\n    "id": 1,\n    "rating": "veryGood",\n    "series": "The Riftwar Saga",\n    "title": "Magician Apprentice",\n    "year_published": 1982\n  },\n  "2": {\n    "author": "Richard Knaak",\n    "id": 2,\n    "rating": "veryGood",\n    "series": "Legends of the Dragonream",\n    "title": "Firedrake",\n    "year_published": 2000\n  },\n  "3": {\n    "author": "Aaron Blabey",\n    "id": 3,\n    "rating": "amazing",\n    "series": "N/A",\n    "title": "Thelma la licorne",\n    "year_published": 2017\n  }\n}\n', response.content.decode("utf-8"))
	def test_bookById(self):
                response = requests.get("http://" + IP + ":" + PORT + "/" + "books" + "/"+ "1")
                self.assertEqual(200, response.status_code)
                self.assertIn('{\n  "author": "Raymond Feist",\n  "id": 1,\n  "rating": "veryGood",\n  "series": "The Riftwar Saga",\n  "title": "Magician Apprentice",\n  "year_published": 1982\n}\n', response.content.decode("utf-8"))
